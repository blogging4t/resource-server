package ro.cpatrut.auth.resourceserver.api;

import com.google.common.collect.Lists;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.cpatrut.auth.resourceserver.dto.MessageTO;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(ProtectedResource.PROTECTED_RESOURCE_PATH)
public class ProtectedResource {
    static final String PROTECTED_RESOURCE_PATH = "/protected-resource";
    private static final List<MessageTO> messages = Lists.newArrayList();

    @GetMapping
    public ResponseEntity<List<MessageTO>> getMessageList() {
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<MessageTO> saveMessage(
            @Valid @RequestBody final MessageTO message) {
        messages.add(message);
        return new ResponseEntity<>(messages.get(messages.size() - 1), HttpStatus.OK);
    }
}
