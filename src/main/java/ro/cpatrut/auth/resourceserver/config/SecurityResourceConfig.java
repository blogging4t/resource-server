package ro.cpatrut.auth.resourceserver.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import static com.google.common.base.Preconditions.checkNotNull;

@Configuration
public class SecurityResourceConfig extends WebSecurityConfigurerAdapter {

    private final String clientId;
    private final String clientSecret;
    private final String introspectionUrl;

    SecurityResourceConfig(
            @Value("${oauth.clientId}") final String clientId,
            @Value("${oauth.clientSecret}") final String clientSecret,
            @Value("${oauth.introspectionUrl}") final String introspectionUrl) {
        this.clientId = checkNotNull(clientId);
        this.clientSecret = checkNotNull(clientSecret);
        this.introspectionUrl = checkNotNull(introspectionUrl);
    }

    @Override
    public void configure(final HttpSecurity http) throws Exception {
        http.authorizeRequests(authz -> authz
                .antMatchers(HttpMethod.GET, "protected-resource").hasAnyAuthority("SCOPE_read")
                .antMatchers(HttpMethod.POST, "protected-resource").hasRole("ADMIN")
                .anyRequest().permitAll())
                .oauth2ResourceServer(oauth2 -> oauth2
                        .opaqueToken(token -> token.introspectionUri(introspectionUrl)
                                .introspectionClientCredentials(clientId, clientSecret)
                        )
                );
    }

}
