FROM openjdk:11-slim

ADD target/resource-server-0.0.1-SNAPSHOT.jar resource-server.jar
COPY run-java.sh run-java.sh
ENTRYPOINT JAVA_APP_JAR=resource-server.jar ./run-java.sh